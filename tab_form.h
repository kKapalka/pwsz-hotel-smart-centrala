#ifndef TAB_FORM_H
#define TAB_FORM_H

#include <QMainWindow>
#include "smart_hotel.h"

namespace Ui {
class tab_form;
}

class tab_form : public QMainWindow
{
    Q_OBJECT

public:
    explicit tab_form(smart_hotel *parent);
    ~tab_form();
    void ui_set(int roomid, int (*rooms_tab2)[9]);

    smart_hotel *parent2 = 0;
    int rnum;
private slots:
    void on_horizontalSlider_valueChanged(int value);
    void on_horizontalSlider_3_valueChanged(int value);
    void on_pb_light_clicked();
    void on_pb_temp_clicked();
private:
    Ui::tab_form *ui;
};

#endif // TAB_FORM_H
