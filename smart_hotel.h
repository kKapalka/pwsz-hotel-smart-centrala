#ifndef SMART_HOTEL_H
#define SMART_HOTEL_H

#include <QMainWindow>
#include <QSerialPort>
#include <QMessageBox>
#include <QTimer>

namespace Ui {
class smart_hotel;
}

class smart_hotel : public QMainWindow
{
    Q_OBJECT

public:
    explicit smart_hotel(QWidget *parent = 0);
    ~smart_hotel();
    void set_values(int mod, int value, int roomid);
    QSerialPort *serial = new QSerialPort;

    #define M       300
    #define N       9
    #define IDLE    60000   //set idle time to 60s
    QTimer *rsynctimer = new QTimer;
    QTimer *inactivity = new QTimer;
    bool user_idle_trigger = 0;
    QList <int>fast_roomsscan;
    QString leer_getdata;
    QStringList splitted_data;
    int rooms_tab[M][N];
    int leer_prepareddata[N-1];
    int rooms_count = 0;

private slots:
    void on_b_port_open_clicked();
    void on_b_port_close_clicked();
    void leer_serial();
    void on_actionZamknij_triggered();
    void on_pushButton_3_clicked();
    void add_room_clicked();
    void delete_room_clicked();
    void on_tab_parter_currentChanged(int index);    
    void on_tab_p1_currentChanged(int index);
    void on_tab_p2_currentChanged(int index);
    void ex_th_tick();
    void on_pushButton_clicked();
    void warn_update();
    void set_inactive();
    void set_active();
private:
    void scan_rooms();
    Ui::smart_hotel *ui;
    QMessageBox *mes1 = new QMessageBox;
    void mouseReleaseEvent(QMouseEvent*);
    void keyPressEvent(QKeyEvent*);
};

#endif // SMART_HOTEL_H
